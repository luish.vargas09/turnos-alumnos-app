const URL = 'http://mac7.onthewifi.com/';
////Crear Cookie
//document.cookie = `nombrecookie=valorcookie; max-age=3600; path=/`;
const deleteCookies = () => {
    let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i];
        let eqPos = cookie.indexOf('=');
        let name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
    }
};
//
const getCookie = (name) => {
    let nameEQ = name + '=';
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) {
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
    }
    return null;
};
//Corroborar sesión iniciada
const idUsuario = getCookie("idUsuario");
if (idUsuario == null) {
    window.location = "/login";
} else {
    //Validación de si es admin o docente
    fetch(`${URL}usuariosRest/${idUsuario}`)
            .then((response) => response.json())
            .then((doc) => {
                if (doc.tipo == 'profesor') {
                    window.location = "/profesor";
                }
            });
}

/*
 * 
 * 
 */
let cerrarSesionButton;
let profesoresTBody;
let actualizarInput;
let cubiculosArray;
let turnosTBody;
let cancelarTodoInput;
//
cerrarSesionButton = document.getElementById('cerrarSesionButton');
profesoresTBody = document.getElementById('profesoresTBody');
actualizarInput = document.getElementById('actualizarInput');
turnosTBody = document.getElementById('turnosTBody');
cancelarTodoInput = document.getElementById('cancelarTodoInput');
//
cerrarSesionButton.addEventListener('click', () => {
    deleteCookies();
    window.location = "/login";
});
//
actualizarInput.addEventListener('click', () => {
    getListaProfesores();
});
//
cancelarTodoInput.addEventListener('click', () => {
    fetch(`${URL}turnosRest/`)
            .then((response) => response.json())
            .then((doc) => {
                doc.forEach((doc) => {
                    if (doc.estado == 'pendiente') {
                        const datosConst = {
                            id: doc.id,
                            matricula: doc.matricula,
                            estado: 'cancelado',
                            fecha: doc.fecha,
                            id_usuario: {
                                id: doc.id_usuario.id,
                                correo: doc.id_usuario.correo,
                                password: doc.id_usuario.password,
                                tipo: doc.id_usuario.tipo,
                                nombre: doc.id_usuario.nombre,
                                apellido: doc.id_usuario.apellido,
                                id_cubiculo: {
                                    id: doc.id_usuario.id_cubiculo.id,
                                    clave: doc.id_usuario.id_cubiculo.clave
                                }
                            },
                            id_materia: {
                                id: doc.id_materia.id,
                                nombre: doc.id_materia.nombre
                            }
                        }
                        const opcionsConst = {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(datosConst)
                        };
                        fetch(`${URL}turnosRest/${doc.id}`, opcionsConst)
                                .then(response => response.json())
                                .then(doc => {
                                    console.log(`Turno ${doc.id_usuario.id_cubiculo.clave}-${doc.id} atendido`);
                                });
                    }
                });
                //Aquí se cierra la sesión
                getTurnos();
            });
});
//
const getListaProfesores = () => {
    fetch(`${URL}cubiculosRest/`)
            .then(response => response.json())
            .then(doc => {
                cubiculosArray = [];
                doc.forEach(doc => {
                    if (doc.clave != 'Dirección') {
                        cubiculosArray.push({
                            id: doc.id,
                            clave: doc.clave
                        });
                    }
                });
                fetch(`${URL}usuariosRest/`)
                        .then(response => response.json())
                        .then(doc => {
                            profesoresTBody.innerHTML = ``;
                            doc.forEach(doc => {
                                if (doc.tipo == 'profesor') {
                                    profesoresTBody.innerHTML += `
                                        <tr>
                                            <td>${doc.nombre}</td>
                                            <td>${doc.apellido}</td>
                                            <td>${doc.correo}</td>
                                            <td>${doc.password}</td>
                                            <td>${doc.id_cubiculo.clave}</td>
                                            <td>
                                                <button id="editUsuario${doc.id}Button" type="button" class="btn btn-secondary" data-bs-toggle="modal" data-bs-target="#editUsuario${doc.id}Modal">
                                                    Editar    
                                                </button>
                                            </td>
                                        </tr>
                                        <!-- Modal -->
                                        <div class="modal fade" id="editUsuario${doc.id}Modal" tabindex="-1" aria-hidden="true">
                                          <div class="modal-dialog modal-xl">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title">${doc.nombre + ' ' + doc.apellido}</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                              </div>
                                              <div class="modal-body" style="margin: 0px; padding: 0px;">     
                                                <div class="row" style="margin: 0px; padding: 0px;">
                                                        <div class="col-2">Nombre(s)</div>
                                                        <div class="col-2">Apellido(s)</div>
                                                        <div class="col-3">Correo</div>
                                                        <div class="col-2">Contraseña</div>
                                                        <div class="col-1"></div>
                                                        <div class="col-2">Cubículo</div>
                                                        <hr>
                                                    <div class="col-2">
                                                        <input id="nombre${doc.id}" value="${doc.nombre}">
                                                    </div>
                                                    <div class="col-2">
                                                        <input id="apellido${doc.id}" value="${doc.apellido}">
                                                    </div>
                                                    <div class="col-3">
                                                        <input id="correo${doc.id}" value="${doc.correo}">
                                                    </div>
                                                    <div class="col-2">
                                                        <input id="password${doc.id}" value="${doc.password}">
                                                    </div>
                                                    <div class="col-1"></div>
                                                    <div class="col-2">
                                                        <select id="cubiculoUsuarioSelect${doc.id}"></select>
                                                    </div>
                                                </div>      
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                                                <button id="guardarCambiosButton${doc.id}" type="button" class="btn btn-success" data-bs-dismiss="modal">Guardar cambios</button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    `;
                                }
                            });
                            doc.map(doc => {
                                if (doc.tipo == 'profesor') {
                                    //console.log(cubiculosArray);
                                    let cubiculoUsuarioSelect = document.getElementById(`cubiculoUsuarioSelect${doc.id}`);
                                    let cubiculoIdActual = doc.id_cubiculo.id;
                                    //cubiculoUsuarioSelect.innerHTML = ``;
                                    cubiculosArray.forEach(doc => {
                                        if (cubiculoIdActual == doc.id) {
                                            cubiculoUsuarioSelect.innerHTML += `<option selected value="${doc.id}">${doc.clave}</option>`;
                                        } else {
                                            cubiculoUsuarioSelect.innerHTML += `<option value="${doc.id}">${doc.clave}</option>`;
                                        }
                                    });
                                    //Agregar los identificadores de los INPUT y función de botón Guardar
                                    let nombre = document.getElementById(`nombre${doc.id}`);
                                    let apellido = document.getElementById(`apellido${doc.id}`);
                                    let correo = document.getElementById(`correo${doc.id}`);
                                    let password = document.getElementById(`password${doc.id}`);
                                    //cubiculoUsuarioSelect
                                    let guardarCambiosButton = document.getElementById(`guardarCambiosButton${doc.id}`);
                                    //addEventListener
                                    guardarCambiosButton.addEventListener('click', () => {
                                        //No olvidar SobreEscribir el cubículo si es ocupado por otro
                                        /*
                                         "id": 1,
                                         "correo": "luishumbertovargassanchez@uteq.edu.mx",
                                         "password": "1234",
                                         "tipo": "profesor",
                                         "nombre": "Luis Humberto",
                                         "apellido": "Vargas Sánchez",
                                         "id_cubiculo": {
                                         "id": 1,
                                         "clave": "K1"
                                         } 
                                         */
                                        console.log(correo.value)
                                        console.log(cubiculoUsuarioSelect.value)
                                        const datosUpload = {
                                            id: doc.id,
                                            correo: correo.value,
                                            password: password.value,
                                            tipo: "profesor",
                                            nombre: nombre.value,
                                            apellido: apellido.value,
                                            id_cubiculo: {
                                                id: cubiculoUsuarioSelect.value
                                            }
                                        };
                                        const options = {
                                            method: 'POST',
                                            headers: {
                                                'Content-Type': 'application/json'
                                            },
                                            body: JSON.stringify(datosUpload)
                                        };
                                        fetch(`${URL}usuariosRest/`, options)
                                                .then(response => response.json())
                                                .then(doc => {
                                                    //console.log('TOdo ok');
                                                    getListaProfesores()
                                                }).catch((error) => {
                                            console.log(error);
                                        });
                                    });
                                }
                            });
                        });
            });
};
getListaProfesores();
//
const getTurnos = () => {
    fetch(`${URL}turnosRest/`)
            .then(doc => doc.json())
            .then(doc => {
                turnosTBody.innerHTML = '';
               doc.reverse().forEach(doc=>{
                   turnosTBody.innerHTML += `
                        <tr>
                            <td>${doc.id_usuario.nombre} ${doc.id_usuario.apellido}</td>
                            <td>${doc.id_usuario.id_cubiculo.clave}-${doc.id}</td>
                            <td>${doc.matricula}</td>
                            <td>${doc.estado}</td>
                        </tr>
                    `;
               });
            });
};
getTurnos();
//
window.setInterval(() => {
    getTurnos();
}, 3000);