/* global fetch */
//Variables y constantesKua?
const URL = 'http://mac7.onthewifi.com/';
let sacarTurnoInput;
let segundaColDiv;
let regresarInicioInput;
let listaTBody;
let materiaSelect;
let docenteSelectDiv;
let docenteSelect;
let addTurnoDiv;
let addTurnoInput;
let matriculaInput;
let codigoTurno;
let temp;
let successInput;
let turnosArray;
let agregador;
let cubiculosExistentesArray;
//
segundaColDiv = document.getElementById('segundaColDiv');
listaTBody = document.getElementById('listaTBody');
//
const getCubiculos = () => {
    listaTBody.innerHTML = `
        <tr>
            <td>Wait...</td>
            <td>Wait...</td>
            <td>Wait...</td>
        </tr>
    `;
    fetch(`${URL}cubiculosRest/`)
            .then((response) => response.json())
            .then((doc) => {
                listaTBody.innerHTML = ``;
                cubiculosExistentesArray = [];
                doc.forEach((doc) => {
                    if (doc.clave != 'Dirección') {
                        cubiculosExistentesArray.push(doc);
                        listaTBody.innerHTML += `
                        <tr>
                            <td id="docenteTR${doc.id}">-</td>
                            <td>${doc.clave}</td>
                            <td id="turnoTR${doc.id}">-</td>
                        </tr>
                    `;
                    }
                });
                console.log(cubiculosExistentesArray);
            });
};
getCubiculos();
//
const sacarTurnoFunction = () => {
    segundaColDiv.innerHTML = `
        <div class="sacarTurnoDiv">
            <div class="sacarTurnoHijo">
                <input id="sacarTurnoInput" class="btn btn-success sacarTurnoBtn" type="button" value="TURNO ✋"/>
            </div>
        </div>
        `;
    sacarTurnoInput = document.getElementById('sacarTurnoInput');
    sacarTurnoInput.addEventListener('click', () => {
        segundaColDiv.innerHTML = `
            <input id="regresarInicioInput" class="btn btn-danger regresarInput" type="button" value="⇠"/>
            <br>
            <center>
                <input id="matriculaInput" class="matriculaInput" placeholder="Matrícula" type="number">
            </center>
            <br>
            <select class="form-select" id="materiaSelect"></select>
            <div id="docenteSelectDiv"></div>
            <div id="addTurnoDiv"></div>
        `;
        regresarInicioInput = document.getElementById('regresarInicioInput');
        materiaSelect = document.getElementById('materiaSelect');
        //
        getMaterias();
        //
        regresarInicioInput.addEventListener('click', () => {
            sacarTurnoFunction();
        });
        //
        materiaSelect.addEventListener('change', () => {
            getDocentes();
        });
    });
};
sacarTurnoFunction();
//
const getMaterias = () => {
    materiaSelect = document.getElementById('materiaSelect');
    materiaSelect.innerHTML = `
        <option value="0" selected>Wait...</option>
    `;
    fetch(`${URL}materiasRest/`)
            .then((response) => response.json())
            .then((doc) => {
                materiaSelect.innerHTML = `
                    <option value="0" selected>Seleccione materia</option>
                `;
                doc.forEach((doc) => {
                    materiaSelect.innerHTML += `
                        <option value="materiaId${doc.id}">${doc.nombre}</option>
                    `;
                });
            });
};
//
const getDocentes = () => {
    docenteSelectDiv = document.getElementById('docenteSelectDiv');
    addTurnoDiv = document.getElementById('addTurnoDiv');
    addTurnoDiv.innerHTML = ``;
    if (materiaSelect.value == 0) {
        docenteSelectDiv.innerHTML = ``;
        return;
    }
    docenteSelectDiv.innerHTML = `
        <select class="form-select" id="docenteSelect"></select>
    `;
    docenteSelect = document.getElementById('docenteSelect');
    docenteSelect.innerHTML = `
        <option value="0" selected>Wait...</option>
    `;
    fetch(`${URL}docentesMateriasRest/`)
            .then((response) => response.json())
            .then((doc) => {
                docenteSelect.innerHTML = `
                    <option value="0" selected>Seleccione docente</option>
                `;
                //console.log(materiaSelect.value.substr(9));//materiaId1
                doc.forEach((doc) => {
                    if (materiaSelect.value.substr(9) == doc.id_materia.id) {
                        docenteSelect.innerHTML += `
                            <option value="docenteId${doc.id_usuario.id}">${doc.id_usuario.nombre} ${doc.id_usuario.apellido}</option>
                        `;
                    }
                });
                docenteSelect.addEventListener('change', () => {
                    if (docenteSelect.value == 0) {
                        addTurnoDiv.innerHTML = ``;
                        return;
                    }
                    addTurnoDiv.innerHTML = `
                        <hr>
                        <center>
                            <input id="addTurnoInput" class="btn btn-success successInput" value="Solicitar turno" type="button">
                        </center>
                    `;
                    //
                    addTurnoInput = document.getElementById('addTurnoInput');
                    matriculaInput = document.getElementById('matriculaInput');
                    //
                    addTurnoInput.addEventListener('click', () => {
                        //Validación
                        if (matriculaInput.value.toString().length != 10) {
                            matriculaInput.focus();
                            matriculaInput.style.borderBottom = 'solid red 3px';
                            window.setTimeout(() => {
                                matriculaInput.style.borderBottom = 'solid 3px #198754';
                            }, 1000);
                            return;
                        }
                        addTurnoFunction(matriculaInput.value, materiaSelect.value.substr(9), docenteSelect.value.substr(9));
                    });
                });
            });
};
//
const addTurnoFunction = (matricula, materia, docente) => {
    const datosUpload = {
        id: 0,
        matricula: matricula,
        estado: "pendiente",
        id_materia: {
            id: materia
        },
        id_usuario: {
            id: docente
        }
    };
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(datosUpload)
    };
    fetch(`${URL}turnosRest/`, options)
            .then((response) => response.json())
            .then((doc) => {
                if (doc !== null) {
                    console.log('Guardado');
                    //location.href = '/';
                    //sacarTurnoFunction();
                    fetch(`${URL}usuariosRest/`)
                            .then((response) => response.json())
                            .then((doc) => {
                                doc.forEach((doc) => {
                                    if (docente == doc.id) {
                                        codigoTurno = doc.id_cubiculo.clave;
                                    }
                                });
                                fetch(`${URL}turnosRest/`)
                                        .then((response) => response.json())
                                        .then((doc) => {
                                            doc.forEach((doc) => {
                                                temp = doc.id;
                                            });
                                            /* CON ESTO SE PUEDE PONER DE REVERSA
                                             doc.reverse().forEach((doc)=>{
                                             console.log(doc.id);
                                             }); 
                                             */
                                            //Aquí te dice cuál es tu turno
                                            console.log(codigoTurno + '-' + temp);
                                            segundaColDiv.innerHTML = `
                                                <center>
                                                    <h3 class="turnoH3">TURNO</h2>
                                                    <h1 class="turnoH3">${codigoTurno}-${temp}</h1>
                                                    <hr>
                                                    <input id="successInput" class="btn btn-success successInput" type="button" value="OK">
                                                </center>
                                            `;
                                            successInput = document.getElementById('successInput');
                                            successInput.addEventListener('click', () => {
                                                sacarTurnoFunction();
                                            });
                                            getTurnos();
                                        });
                                //
                            });
                } else {
                    console.log(doc);
                    throw new Error('We have problem Houston!');
                }
            })
            .catch((error) => {
                console.log(error);
            });
};
//
const getTurnos = () => {
    fetch(`${URL}turnosRest/`)
            .then((response) => response.json())
            .then((doc) => {
                turnosArray = [{idCubiculo: '.', cubiculo: '.', turno: '.', docente: '.'}];
                //doc.reverse().forEach((doc) => {
                doc.forEach((doc) => {
                    if (doc.estado == 'pendiente') {
                        //Aquí ya se obtienen todos los pendientes
                        //console.log(doc.id_usuario.id_cubiculo.clave+ '-' + doc.id);
                        agregador = true;
                        turnosArray.forEach((data) => {
                            //console.log(data);
                            if (data.cubiculo == doc.id_usuario.id_cubiculo.clave) {
                                agregador = false;
                                //console.log(doc.id_usuario.id_cubiculo.clave);
                                //console.log(doc.id_usuario.nombre + doc.id_usuario.apellido);
                            }
                        });
                        if (agregador == true) {
                            agregador = false;
                            turnosArray.push({
                                idCubiculo: doc.id_usuario.id_cubiculo.id,
                                cubiculo: doc.id_usuario.id_cubiculo.clave,
                                turno: doc.id_usuario.id_cubiculo.clave + '-' + doc.id,
                                docente: doc.id_usuario.nombre + ' ' + doc.id_usuario.apellido,
                            });
                        }
                    }
                });
                //Aquí se muestran en pantalla
                //console.log(turnosArray);
                //Limpieza
                cubiculosExistentesArray.forEach((doc) => {
                    let turnoTR = document.getElementById(`turnoTR` + doc.id);
                    let docenteTR = document.getElementById(`docenteTR` + doc.id);
                    turnoTR.innerHTML = `-`;
                    docenteTR.innerHTML = `-`;
                });
                turnosArray.forEach((doc) => {
                    if (doc.idCubiculo != '.') {
                        let turnoTR = document.getElementById(`turnoTR` + doc.idCubiculo);
                        let docenteTR = document.getElementById(`docenteTR` + doc.idCubiculo);
                        turnoTR.innerHTML = `${doc.turno}`;
                        docenteTR.innerHTML = `${doc.docente}`;
                        //
                        //cubiculosExistentesArray
                    }
                });
            });
};
getTurnos();
//
window.setInterval(() => {
    getTurnos();
}, 3000);