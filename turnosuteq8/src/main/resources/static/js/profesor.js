const URL = 'http://mac7.onthewifi.com/';
////Crear Cookie
//document.cookie = `nombrecookie=valorcookie; max-age=3600; path=/`;
const deleteCookies = () => {
    let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
        let cookie = cookies[i];
        let eqPos = cookie.indexOf('=');
        let name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
    }
};
//
const getCookie = (name) => {
    let nameEQ = name + '=';
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) {
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
    }
    return null;
};
//Corroborar sesión iniciada
const idUsuario = getCookie("idUsuario");
if (idUsuario == null) {
    window.location = "/login";
} else {
    //Validación de si es admin o docente
    fetch(`${URL}usuariosRest/${idUsuario}`)
            .then((response) => response.json())
            .then((doc) => {
                if (doc.tipo == 'admin') {
                    window.location = "/admin";
                }
            });
}

/*
 * 
 * 
 */
let profesorDisplay;
let cubiculoDisplay;
let turnoDisplay;
let matriculaDisplay;
let pendientesDisplay;
let siguienteButton;
let finalizarButton;
let turnoActual;
let cubiculoClave;
let contadorPendientes;
let historialTBody;
//
profesorDisplay = document.getElementById('profesorDisplay');
cubiculoDisplay = document.getElementById('cubiculoDisplay');
turnoDisplay = document.getElementById('turnoDisplay');
matriculaDisplay = document.getElementById('matriculaDisplay');
pendientesDisplay = document.getElementById('pendientesDisplay');
siguienteButton = document.getElementById('siguienteButton');
finalizarButton = document.getElementById('finalizarButton');
historialTBody = document.getElementById('historialTBody');
//
siguienteButton.addEventListener('click', () => {
    putSiguiente(turnoActual.idTurno, 'atendido');
});
finalizarButton.addEventListener('click', () => {
    fetch(`${URL}turnosRest/`)
            .then((response) => response.json())
            .then((doc) => {
                doc.forEach((doc) => {
                    if (doc.id_usuario.id_cubiculo.clave == getCookie('cubiculoClave') && doc.estado == 'pendiente') {
                        const datosConst = {
                            id: doc.id,
                            matricula: doc.matricula,
                            estado: 'saltado',
                            fecha: doc.fecha,
                            id_usuario: {
                                id: doc.id_usuario.id,
                                correo: doc.id_usuario.correo,
                                password: doc.id_usuario.password,
                                tipo: doc.id_usuario.tipo,
                                nombre: doc.id_usuario.nombre,
                                apellido: doc.id_usuario.apellido,
                                id_cubiculo: {
                                    id: doc.id_usuario.id_cubiculo.id,
                                    clave: doc.id_usuario.id_cubiculo.clave
                                }
                            },
                            id_materia: {
                                id: doc.id_materia.id,
                                nombre: doc.id_materia.nombre
                            }
                        }
                        const opcionsConst = {
                            method: 'PUT',
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            body: JSON.stringify(datosConst)
                        };
                        fetch(`${URL}turnosRest/${doc.id}`, opcionsConst)
                                .then(response => response.json())
                                .then(doc => {
                                    console.log(`Turno ${doc.id_usuario.id_cubiculo.clave}-${doc.id} atendido`);
                                });
                    }
                });
                //Aquí se cierra la sesión
                deleteCookies();
                window.location = "/login";
            });
});
//
const getInfo = (id) => {
    fetch(`${URL}usuariosRest/${id}`)
            .then(response => response.json())
            .then(doc => {
                profesorDisplay.value = doc.nombre + ' ' + doc.apellido;
                cubiculoDisplay.value = doc.id_cubiculo.clave;
                document.cookie = `idUsuario=${doc.id}; max-age=3600; path=/`;
                document.cookie = `cubiculoClave=${doc.id_cubiculo.clave}; max-age=3600; path=/`;
            });
};
getInfo(getCookie("idUsuario"));
//
const getTurnos = (id) => {
    fetch(`${URL}turnosRest/`)
            .then((response) => response.json())
            .then((doc) => {
                turnoActual = {
                    idTurno: '-',
                    display: '-',
                    matricula: '-'
                };
                contadorPendientes = 0;
                doc.reverse().forEach((doc) => {
                    if (doc.id_usuario.id_cubiculo.clave == getCookie('cubiculoClave') && doc.estado == 'pendiente') {
                        turnoActual = {
                            idTurno: doc.id,
                            display: doc.id_usuario.id_cubiculo.clave + '-' + doc.id,
                            matricula: doc.matricula
                        };
                        contadorPendientes++;
                    }
                });
                turnoDisplay.value = turnoActual.display;
                matriculaDisplay.value = turnoActual.matricula;
                pendientesDisplay.value = `${contadorPendientes}`;
                //console.log(turnoActual);
            });
};
getTurnos(getCookie("idUsuario"));
//
const putSiguiente = (idTurno, estadoTurno) => {
    fetch(`${URL}turnosRest/${idTurno}`)
            .then(response => response.json())
            .then(doc => {
                const datosConst = {
                    id: idTurno,
                    matricula: doc.matricula,
                    estado: estadoTurno,
                    fecha: doc.fecha,
                    id_usuario: {
                        id: doc.id_usuario.id,
                        correo: doc.id_usuario.correo,
                        password: doc.id_usuario.password,
                        tipo: doc.id_usuario.tipo,
                        nombre: doc.id_usuario.nombre,
                        apellido: doc.id_usuario.apellido,
                        id_cubiculo: {
                            id: doc.id_usuario.id_cubiculo.id,
                            clave: doc.id_usuario.id_cubiculo.clave
                        }
                    },
                    id_materia: {
                        id: doc.id_materia.id,
                        nombre: doc.id_materia.nombre
                    }
                }
                const opcionsConst = {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(datosConst)
                };
                fetch(`${URL}turnosRest/${idTurno}`, opcionsConst)
                        .then(response => response.json())
                        .then(doc => {
                            //console.log('A ver checa si sí furuló');
                            getTurnos(getCookie("idUsuario"));
                            getHistorial(getCookie("idUsuario"));
                        });
            }
            );
};
//
const getHistorial = (id) => {
    fetch(`${URL}turnosRest/`)
            .then(response => response.json())
            .then(doc => {
                historialTBody.innerHTML = ``;
                doc.reverse().forEach((doc) => {
                    if (doc.id_usuario.id == id) {
                        historialTBody.innerHTML += `
                        <tr>
                            <th scope="row">${doc.id_usuario.id_cubiculo.clave}-${doc.id}</th>
                            <td>${doc.matricula}</td>
                            <td>${doc.estado}</td>
                        </tr>        
                    `;
                        //console.log(doc);
                    }
                });
            });
};
//
getHistorial(getCookie("idUsuario"));
//
window.setInterval(() => {
    getTurnos(getCookie("idUsuario"));
    getHistorial(getCookie("idUsuario"));
}, 3000);