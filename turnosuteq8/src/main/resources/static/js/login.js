const URL = 'http://mac7.onthewifi.com/';
////Crear Cookie
//document.cookie = `nombrecookie=valorcookie; max-age=3600; path=/`;
const getCookie = (name) => {
    let nameEQ = name + '=';
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) {
            return decodeURIComponent(c.substring(nameEQ.length, c.length));
        }
    }
    return null;
};
//Corroborar sesión iniciada
const idUsuario = getCookie("idUsuario");
if (idUsuario != null) {
    //Validación de si es admin o docente
    fetch(`${URL}usuariosRest/${idUsuario}`)
            .then((response) => response.json())
            .then((doc) => {
                if (doc.tipo == 'profesor') {
                    window.location = "/profesor";
                }
                if (doc.tipo == 'admin') {
                    window.location = "/admin";
                }
            });
}
/*
 * 
 * 
 */
let correoInput;
let passwordInput;
let loginInput;
//
correoInput = document.getElementById('correoInput');
passwordInput = document.getElementById('passwordInput');
loginInput = document.getElementById('loginInput');
//
loginInput.addEventListener('click', () => {
    if (correoInput.value.includes('@uteq.edu.mx')) {
        fetch(`${URL}usuariosRest/`)
                .then(response => response.json())
                .then((doc) => {
                    console.log('\nValidando...');
                    doc.forEach((doc) => {
                        if (doc.correo == correoInput.value) {
                            console.log('Correo coincide');
                            if (doc.password == passwordInput.value) {
                                console.log('Contraseña coincide');
                                document.cookie = `idUsuario=${doc.id}; max-age=3600; path=/`;
                                document.cookie = `cubiculoClave=${doc.id_cubiculo.clave}; max-age=3600; path=/`;
                                location.reload();
                            } else {
                                passwordInput.focus();
                                correoInput.style.border = 'solid red 5px';
                                passwordInput.style.border = 'solid red 5px';
                                window.setTimeout(() => {
                                    correoInput.style.border = 'solid grey 1px';
                                    passwordInput.style.border = 'solid grey 1px';
                                }, 1000);
                            }
                        } else {
                            correoInput.focus();
                            correoInput.style.border = 'solid red 5px';
                            passwordInput.style.border = 'solid red 5px';
                            window.setTimeout(() => {
                                correoInput.style.border = 'solid grey 1px';
                                passwordInput.style.border = 'solid grey 1px';
                            }, 1000);
                        }
                    });
                });
    } else {
        correoInput.focus();
        correoInput.style.border = 'solid red 5px';
        window.setTimeout(() => {
            correoInput.style.border = 'solid grey 1px';
        }, 1000);
    }

});