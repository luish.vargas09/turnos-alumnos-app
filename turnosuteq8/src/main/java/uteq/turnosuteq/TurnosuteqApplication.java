package uteq.turnosuteq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurnosuteqApplication {

	public static void main(String[] args) {
		SpringApplication.run(TurnosuteqApplication.class, args);
	}

}
