/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uteq.turnosuteq.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

/**
 *
 * @author Jesus
 */
@Entity
@Data
@Table(name = "turnos")
public class TurnosModel implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    
    @Column(name = "matricula")
    private String matricula;
    
    @Column(name = "estado")
    private String estado;
    
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @PrePersist
    public void prePersist() {
        fecha = new Date();
    }
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "id_docente")
    private UsuariosModel id_usuario;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "id_materia")
    private MateriasModel id_materia;
}
