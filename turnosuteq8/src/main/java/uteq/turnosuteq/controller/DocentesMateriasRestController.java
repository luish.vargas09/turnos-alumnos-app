/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package uteq.turnosuteq.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import uteq.turnosuteq.model.entity.DocentesMateriasModel;
import uteq.turnosuteq.model.repository.DocentesMateriasRepository;

/**
 *
 * @author Jesus
 */
@RestController
@RequestMapping("/docentesMateriasRest")
public class DocentesMateriasRestController {
    
   @Autowired
    private DocentesMateriasRepository docentesMateriasRepository;
    
    @CrossOrigin(origins = "*")
    @GetMapping()
    public List<DocentesMateriasModel> list() {
        return docentesMateriasRepository.findAll();
    }
    
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public DocentesMateriasModel get(@PathVariable Long id) {
        return docentesMateriasRepository.findById(id).orElse(null);
    }
    
    @CrossOrigin(origins = "*")
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody DocentesMateriasModel input) {
        
        DocentesMateriasModel docenteMateriaActual = docentesMateriasRepository.findById(id).orElse(null);

        if (docenteMateriaActual == null) {
            return new ResponseEntity<>(docenteMateriaActual, HttpStatus.NOT_FOUND);
        }

        docenteMateriaActual.setId_usuario(input.getId_usuario());
        docenteMateriaActual.setId_materia(input.getId_materia());

        DocentesMateriasModel docenteMateriaActualizado = docentesMateriasRepository.save(docenteMateriaActual);
        HttpStatus generatedStatus = HttpStatus.CREATED;

        return new ResponseEntity<>(docenteMateriaActualizado, generatedStatus);
    }
    
    @CrossOrigin(origins = "*")
    @PostMapping
    public ResponseEntity<?> post(@RequestBody DocentesMateriasModel input) {
        return new ResponseEntity<>(docentesMateriasRepository.save(input), HttpStatus.CREATED);
    }
    
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        docentesMateriasRepository.deleteById(id);
        return new ResponseEntity<>("Borrado", HttpStatus.OK);
    }
    
}
