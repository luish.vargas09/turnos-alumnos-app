/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package uteq.turnosuteq.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import uteq.turnosuteq.model.entity.CubiculosModel;
import uteq.turnosuteq.model.repository.CubiculosRepository;

/**
 *
 * @author Jesus
 */
@RestController
@RequestMapping("/cubiculosRest")
public class CubiculosRestController {
    
       
   @Autowired
    private CubiculosRepository cubiculosRepository;
    
    @CrossOrigin(origins = "*")
    @GetMapping()
    public List<CubiculosModel> list() {
        return cubiculosRepository.findAll();
    }
    
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public CubiculosModel get(@PathVariable Long id) {
        return cubiculosRepository.findById(id).orElse(null);
    }
    
    @CrossOrigin(origins = "*")
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody CubiculosModel input) {
        
        CubiculosModel cubiculoActual = cubiculosRepository.findById(id).orElse(null);

        if (cubiculoActual == null) {
            return new ResponseEntity<>(cubiculoActual, HttpStatus.NOT_FOUND);
        }

        cubiculoActual.setClave(input.getClave());

        CubiculosModel cubiculoActualizado = cubiculosRepository.save(cubiculoActual);
        HttpStatus generatedStatus = HttpStatus.CREATED;

        return new ResponseEntity<>(cubiculoActualizado, generatedStatus);
    }
    
    @CrossOrigin(origins = "*")
    @PostMapping
    public ResponseEntity<?> post(@RequestBody CubiculosModel input) {
        return new ResponseEntity<>(cubiculosRepository.save(input), HttpStatus.CREATED);
    }
    
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        cubiculosRepository.deleteById(id);
        return new ResponseEntity<>("Borrado", HttpStatus.OK);
    }
    
}
